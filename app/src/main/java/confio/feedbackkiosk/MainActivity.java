package confio.feedbackkiosk;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import org.apache.http.protocol.HTTP;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void veryGood(View view)  {
        Toast.makeText(getApplicationContext(), "Thank You.", Toast.LENGTH_LONG).show();
    }
    public void Good(View view)  {
        Toast.makeText(getApplicationContext(), "Thank You.", Toast.LENGTH_LONG).show();
    }
    public void average(View view)  {
        Toast.makeText(getApplicationContext(), "Thank You.", Toast.LENGTH_LONG).show();
    }
    public void poor(View view)  {
        Toast.makeText(getApplicationContext(), "Thank You.", Toast.LENGTH_LONG).show();
    }

    public void veryBad(View view)  {

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + "0776921476"));
                intent.putExtra("sms_body", "Hello World...");
                startActivity(intent);
        Toast.makeText(getApplicationContext(), "Thank You.", Toast.LENGTH_LONG).show();
    }

}
